/*
Name: Gage Rindt
Clemson Username: grindt
Lab Section Number: 002
Lab Number: 06
TA: Nushrat Humaira, Evan Hastings
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
	  // IMPLEMENT as instructed below
	  /*This is to seed the random generator */
	  srand(unsigned (time(0)));

	  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	  employee employees[10];
	  for (int i=0; i<10; i++) {
		  cout << "Employee #" << i + 1 << endl;
		  cout << "First Name: ";
		  cin >> employees[i].firstName;
		  cout << "Last Name: ";
		  cin >> employees[i].lastName;
		  cout << "Birth Year: ";
		  cin >> employees[i].birthYear;
		  cout << "Hourly Wage: ";
		  cin >> employees[i].hourlyWage;
		  cout << endl;
	  }

	  /*After the array is created and initialzed we call random_shuffle() see the
	   *notes to determine the parameters to pass in.*/
	   random_shuffle(begin(employees), end(employees), &myrandom);

	   /*Build a smaller array of 5 employees from the first five cards of the array created
	    *above*/
		employee newEmployees[5]= { employees[0], employees[1], employees[2], employees[3], employees[4] };


	    /*Sort the new array.  Links to how to call this function is in the specs
	     *provided*/
		 sort(begin(newEmployees), end(newEmployees), &name_order);

	    /*Now print the array below */
		cout << "New Employee List" << endl << "---------------------------" << endl;
		for(int i=0; i<5; i++) {
			cout << "Employee #" << i + 1 << endl;
			cout << "First Name: " << right << setw(10) << newEmployees[i].firstName << endl;
			cout << "Last Name: " << right << setw(11) << newEmployees[i].lastName << endl;
			cout << "Birth Year: " << right << setw(10) << newEmployees[i].birthYear << endl;
			cout << "Hourly Wage: " << right << setw(9) << fixed << setprecision(2) << newEmployees[i].hourlyWage << endl << endl;
		}



	  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
  // IMPLEMENT

	  if(lhs.lastName < rhs.lastName) {
		  return true;
	  } else {
		  return false;
	  }
}
